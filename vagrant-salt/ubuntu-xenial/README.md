# Salt (Masterless) on Ubuntu Xenial

## Instructions

```bash
vagrant up
vagrant provision # if not done in above command
curl localhost:8083
```
