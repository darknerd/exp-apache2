#!/usr/bin/env bash

### FROM EXPERIENCE
# Need to build a gentoo w/ packer and all the official kernel packages

######## Installation
# emerge sys-apps/paxctl
emerge sys-fs/btrfs-progs
emerge app-emulation/docker
echo 'app-emulation/docker overlay -device-mapper' > /etc/portage/package.use/docker

emerge bridge-utils

# container-init device-mapper overlay seccomp -apparmor -aufs -btrfs -hardened -pkcs11

# *   CONFIG_CGROUP_DEVICE:	 is not set when it should be.
#  *   CONFIG_MEMCG:	 is not set when it should be.
#  *   CONFIG_BRIDGE:	 is not set when it should be.
#  *   CONFIG_BRIDGE_NETFILTER:	 is not set when it should be.
#  *   CONFIG_NETFILTER_XT_MATCH_IPVS:	 is not set when it should be.
#  *   CONFIG_USER_NS:	 is not set when it should be.
#  *   CONFIG_CGROUP_PIDS:	 is not set when it should be.
#  *   CONFIG_MEMCG_SWAP: is required if you wish to limit swap usage of containers
#  *   CONFIG_MEMCG_SWAP_ENABLED:	 is not set when it should be.
#  *   CONFIG_BLK_CGROUP: is optional for container statistics gathering
#  *   CONFIG_BLK_DEV_THROTTLING:	 is not set when it should be.
#  *   CONFIG_CFQ_GROUP_IOSCHED:	 is not set when it should be.
#  *   CONFIG_CGROUP_PERF: is optional for container statistics gathering
#  *   CONFIG_CGROUP_HUGETLB:	 is not set when it should be.
#  *   CONFIG_NET_CLS_CGROUP:	 is not set when it should be.
#  *   CONFIG_CFS_BANDWIDTH: is optional for container statistics gathering
#  *   CONFIG_RT_GROUP_SCHED:	 is not set when it should be.
#  *   CONFIG_IP_VS:	 is not set when it should be.
#  *   CONFIG_IP_VS_PROTO_TCP:	 is not set when it should be.
#  *   CONFIG_IP_VS_PROTO_UDP:	 is not set when it should be.
#  *   CONFIG_IP_VS_NFCT:	 is not set when it should be.
#  *   CONFIG_IP_VS_RR:	 is not set when it should be.
#  *   CONFIG_VXLAN:	 is not set when it should be.
#  *   CONFIG_IPVLAN:	 is not set when it should be.
#  *   CONFIG_MACVLAN:	 is not set when it should be.
#  *   CONFIG_DUMMY:	 is not set when it should be.
#  *   CONFIG_CGROUP_NET_PRIO:	 is not set when it should be.
#  *   CONFIG_DM_THIN_PROVISIONING:	 is not set when it should be.
#  *   CONFIG_OVERLAY_FS:	 is not set when it should be.

# https://wiki.gentoo.org/wiki/Network_bridge

### DEBUGGING
#
# rc-service docker zap
# rm -rf /var/lib/docker/*
# rc-service docker start
# cat /var/log/docker.log

# time="2018-08-09T19:52:00.284781201-07:00" level=error msg="Failed to built-in GetDriver graph btrfs /var/lib/docker"
# time="2018-08-09T19:52:00.285697179-07:00" level=error msg="'overlay' not found as a supported filesystem on this host. Please ensure kernel is new enough and has overlay support loaded."

####### Enable/Start Service
if [[ -f /etc/init.d/docker ]]; then
  /etc/init.d/docker start
  rc-update add docker default
elif command -v systemctl > /dev/null; then
  systemctl start docker.service
  systemctl enable docker.service
fi
