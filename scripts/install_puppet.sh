#!/usr/bin/env bash

# Exit if Puppet is installed
command -v puppet > /dev/null && { echo "Puppet is installed! skipping" ; exit 0; }

# Add puppet remote repository
apt-key adv --keyserver "pgp.mit.edu" --recv-keys "EF8D349F"
echo "deb http://apt.puppetlabs.com $(lsb_release -c -s) puppet5" \
  | tee -a /etc/apt/sources.list.d/puppet5.list

# Add puppet packages to list of packages available
sudo apt-get -qq update

# Install puppet packages
apt-get install -y puppet-agent bolt pdk

# Make Binaries Accessible
for ITEM in /opt/puppetlabs/bin/*; do
  # Skip Bolt if Ubuntu 14.04 to mimick package behavior
  if [[ "$(lsb_release -c -s)" == "trusty" ]]; then
    [[ ${ITEM##*/} == bolt ]] && continue
  fi

  # Link to Source
  sudo ln -sf \
    /opt/puppetlabs/puppet/bin/wrapper.sh \
    /usr/local/bin/${ITEM##*/}
done
