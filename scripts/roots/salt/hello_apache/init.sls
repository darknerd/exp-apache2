{% from "hello_apache/map.jinja" import hello_apache with context %}

hello_apache:
  pkg.installed:
    - name: {{ hello_apache.package }}
  file.managed:
    - name: {{ hello_apache.docroot }}/index.html
    - source: salt://hello_apache/files/index.html
