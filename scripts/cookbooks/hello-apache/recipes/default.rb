#
# Cookbook:: hello-apache
# Recipe:: default
#
# Copyright:: 2018, Joaquin Menchaca, All Rights Reserved.

apt_update 'Update the apt cache daily' do
  frequency 86_400
  action :periodic
end

package node['hello-apache']['package']

cookbook_file "#{node['hello-apache']['docroot']}/index.html" do
  source 'index.html'
  action :create
end

service node['hello-apache']['package'] do
  supports status: true, restart: true, reload: true
  action %i(enable start)
end
